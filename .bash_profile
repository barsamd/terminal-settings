# --- Will only run once
if [ -z ${PATHUPDATED+x} ]
    then
        # --- Path setup

        PATHUPDATED="UPDATED"
        export MACPORTS="/opt/local/bin:/opt/local/sbin"

        # Export Path
        export PATH="$MACPORTS:$PATH"


        # --- add color to prompt
        export CLICOLOR=1
        export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd

        # --- custom prompt
        WHITECOLORB="\e[1;37m"
        NORMAL="\[\e[0m\]"


        # --- override cd
        function cd(){ builtin cd "$@" && ls -l; }

        # --- useful grep shortcut
        function cgrep() {
            grep -rHni -C 3 --color=always "$1" ./;
        }

        function xgrep() {
            ORANGE='\033[0;33m'
            NC='\033[0m' # No Color
            
            printf "\n${ORANGE}========================================================== TODO${NC}\n"
            grep -rHn -C 3 --color=always 'TODO' ./;
            printf "\n${ORANGE}========================================================== CONSOLE${NC}\n"
            grep -rHn -C 3 --color=always 'CONSOLE' ./;
            printf "\n${ORANGE}========================================================== console.log${NC}\n"
            grep -rHn -C 3 --color=always 'console.log' ./;
            printf "\n${ORANGE}========================================================== XXX${NC}\n"
            grep -rHn -C 3 --color=always 'XXX' ./;
        }

        # --- Custom help
        function bhelp() {
            WC='\033[0;32m'
            NC='\033[0m'
            printf "\nChange directories upward:\n\t${WC}..${NC} : move up 1 dir\n\t${WC}...${NC} : move up 2 dirs\n\t${WC}....${NC} : move up 3 dirs\n\t... UP TO 7 directories"
            printf "\nList directory:\n\t${WC}ll${NC} : ls -l\n\t${WC}la${NC} : ls -al"
            printf "\nCustom Greps:\n\t${WC}cgrep <word>${NC} : recursively search this directory for the word, showing file and line number with the word in color\n\t${WC}xgrep${NC} : searches this directory recursively for TODO, CONSOLE, and XXX"
        }

        # --- directory changing
        alias ll='ls -l'
        alias la='ls -al'

        alias ..='cd ..;'
        alias ...='cd ../..;'
        alias ....='cd ../../..;'
        alias .....='cd ../../../..;'
        alias ......='cd ../../../../..;'
        alias .......='cd ../../../../../..;'
        alias ........='cd ../../../../../../..;'

        # --- reload the terminal
        alias reload='source ~/.bash_profile'

        # --- printing the directory tree
        alias tree="find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'"
fi


FGCOLOR="\[\033[0;9`expr $RANDOM % 8`m\]"
INFO="$FGCOLOR (:$NORMAL [ `hostname -s` - `date +"%a %b %d %T"` ]"

SCREENWIDTH=`stty size | tail -c 4`
INFOLENGTH=`echo ${#INFO}`
WHITESHIFT=`expr $SCREENWIDTH - $INFOLENGTH + ${#FGCOLOR} + ${#NORMAL}`
SHIFT=""
for i in $(seq 1 1 $WHITESHIFT)
do
    SHIFT="$SHIFT "
done

DIVIDER=$WHITECOLORB
for i in $(seq 1 1 $SCREENWIDTH)
do
    DIVIDER="$DIVIDER-"
done

export PROMPT_COMMAND="reload"
export MYPS='$(echo -n "${PWD/#$HOME/~}" | awk -F "/" '"'"'{
if (NF>6) print $1 "/" $2 "/.../" $(NF-3) "/" $(NF-2) "/" $(NF-1) "/" $NF;
else print $0;}'"'"')'
PS1="\n$DIVIDER\n$SHIFT$INFO\n$FGCOLOR << $NORMAL$(eval 'echo ${MYPS}') $FGCOLOR>>"
PS1="$PS1 $NORMAL"
export PS2="+ "

##
# Your previous /Users/dokhb/.bash_profile file was backed up as /Users/dokhb/.bash_profile.macports-saved_2015-06-29_at_09:47:11
##
