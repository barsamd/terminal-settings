" UNCOMMENT TO USE UCOMPLETEME FOR AUTO WORD COMPLETION WITH TAB
" call ucompleteme#Setup()

" add status line with file name
set laststatus=2
set statusline+=%F

" basic settings
set nu
syntax on
set autoindent
set tabstop=4 shiftwidth=4 softtabstop=4 expandtab

" set search highlighting
set ignorecase smartcase
set incsearch hlsearch
nmap <silent> ,/ :nohls<CR>

" expand undos and ignore unneccisary files in autocomplete
set undolevels=500
set wildignore=*.swp,*.bak,*.pyc,*.class

" Show spacing by using dots
set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.

" correct pasting by toggling stuff off
set pastetoggle=<F2>

" let the mouse scroll
set mouse=a

" re-map window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" fix line jumps on line wraps
nnoremap j gj
nnoremap k gk

" add sudo save after file is opened
cmap w!! w !sudo tee % >/dev/null

" misc
imap ,, <esc>
imap /= // ======================================================
imap <= <! =================================================== <esc>o
imap <! <!--  --><esc>hhhi
